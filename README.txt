INTRODUCTION
============

Weather.com.au provides weather widgets with data pulled from the RSS feeds
provided by Weather.com.au (http://www.weather.com.au/).

With Weather.com.au you can:

 1. Create up to 10 weather widgets.
 2. Configure the display units of the weather data.
 3. Show current weather conditions of an specific city.
 4. Show a 3 day forecast of an specific city.

See https://drupal.org/project/weathercomau for more information


INSTALLATION
============

 1. Install the module.
 2. Set up the module's permissions.
 3. Configure it at admin/config/services/weathercomau.
 4. Go to admin/structure/block and configure your weather widgets!


DEPENDENCIES
============

This module does not depend on any other module.


MAINTAINERS
===========

 *  jorgegc <http://drupal.org/user/834154>
